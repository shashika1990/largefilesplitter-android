package com.janith.largefilesplitter

import android.Manifest
import android.annotation.TargetApi
import android.app.Activity
import android.content.pm.PackageManager
import android.os.Build
import androidx.core.content.ContextCompat

object PermissionManager {

    const val REQUEST_PERMISSION_CAMERA = 201
    const val REQUEST_PERMISSION_WRITE_EXT_STORAGE = 202
    const val REQUEST_PERMISSION_READ_EXT_STORAGE = 203
    const val REQUEST_PERMISSION_LOCATION = 204

    fun checkPermission(permission: String, activity: Activity) : Boolean {
        when (permission) {
            Manifest.permission.CAMERA -> {
                return if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    true
                } else {
                    requestPermission(activity, REQUEST_PERMISSION_CAMERA)
                    false
                }
            }
            Manifest.permission.WRITE_EXTERNAL_STORAGE -> {
                return if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    true
                } else {
                    requestPermission(activity, REQUEST_PERMISSION_WRITE_EXT_STORAGE)
                    false
                }
            }
            Manifest.permission.READ_EXTERNAL_STORAGE -> {
                return if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    true
                } else {
                    requestPermission(activity, REQUEST_PERMISSION_READ_EXT_STORAGE)
                    false
                }
            }
            Manifest.permission.ACCESS_FINE_LOCATION -> {
                return if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                ) {
                    true
                } else {
                    requestPermission(activity, REQUEST_PERMISSION_LOCATION)
                    /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (activity.shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION) ||
                            activity.shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION))
                        {
                            requestPermission(activity, REQUEST_PERMISSION_LOCATION)
                        }
                    }*/
                    false
                }
            }
        }

        return false
    }


    @TargetApi(Build.VERSION_CODES.M)
    private fun requestPermission(activity: Activity, requestCode: Int) {

        when (requestCode) {
            REQUEST_PERMISSION_CAMERA -> {
                activity.requestPermissions(arrayOf(Manifest.permission.CAMERA), REQUEST_PERMISSION_CAMERA)
            }
            REQUEST_PERMISSION_WRITE_EXT_STORAGE -> {
                activity.requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_PERMISSION_WRITE_EXT_STORAGE)
            }
            REQUEST_PERMISSION_READ_EXT_STORAGE -> {
                activity.requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), REQUEST_PERMISSION_READ_EXT_STORAGE)
            }
            REQUEST_PERMISSION_LOCATION -> {
                activity.requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION),
                    REQUEST_PERMISSION_LOCATION
                )
            }
        }
    }
}