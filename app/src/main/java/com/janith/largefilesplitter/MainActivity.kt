package com.janith.largefilesplitter

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.*
import java.io.*


class MainActivity : AppCompatActivity() {

    val REQUEST_PICK_IMAGE = 1001

    /** the maximum size of each file "chunk" generated, in bytes  */
    var chunkSize = (1 * 1024 * 1024).toLong()
    var parts = mutableListOf<String>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_pick_image?.setOnClickListener { pickImageFromGallery() }
        btn_join_parts?.setOnClickListener {
            CoroutineScope(Dispatchers.IO).launch {
                joinParts()
            }
        }
    }


    private suspend fun split(uri: Uri) {
        val origFile = copyFile(uri)
        updateProgress(0)
        setMessage(Operation.SPLIT)

        if (origFile != null) {
            val bin = BufferedInputStream(FileInputStream(origFile))
            val fileSize = origFile.length()

            var partNumber = 0
            var numberOfParts = (fileSize / chunkSize).toInt()
            if (fileSize % chunkSize != 0L) ++numberOfParts

            initProgress(numberOfParts)

            while (partNumber <= numberOfParts) {
                partNumber++
                updateProgress(partNumber)
                val part = createPartFile(applicationContext, partNumber)
                parts.add(part.absolutePath)
                val bout = BufferedOutputStream(FileOutputStream(part))

                for (byte in 0 until chunkSize) {
                    bout.write(bin.read())
                }

                bout.close()
            }
        }

        origFile?.delete()
        showSnackbarAlert(Operation.SPLIT)
    }

    /**
     * Join parts
     */
    private suspend fun joinParts() {
        updateProgress(0)
        setMessage(Operation.JOIN)

        val image = createImageFile(applicationContext)
        val bout = BufferedOutputStream(FileOutputStream(image))

        for ((index, path) in parts.withIndex()) {
            val part = File(path)
            val bin = BufferedInputStream(FileInputStream(part))

            var byte = 0
            while (bin.read().also { byte = it } != -1) {
                bout.write(byte)
            }

            bin.close()
            part.delete()
            updateProgress(index + 1)
        }

        bout.close()
        showSnackbarAlert(Operation.JOIN)
    }


    @Throws(IOException::class)
    fun createPartFile(context: Context, partNumber: Int): File {
        val imageFileName = "chunk_$partNumber"
        val storageDir = context.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS)

        return File.createTempFile(
                imageFileName,
                ".chunk",
                storageDir
        )
    }

    @Throws(IOException::class)
    fun createImageFile(context: Context): File {
        val imageFileName = "full_image"
        val storageDir = context.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS)

        return File.createTempFile(
            imageFileName,
            ".mp4",
            storageDir
        )
    }

    @Throws(IOException::class)
    fun createFile(context: Context): File {
        val imageFileName = "original_file"
        val storageDir = context.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS)

        return File.createTempFile(
                imageFileName,
                ".chunk",
                storageDir
        )
    }

    fun copyFile(uri: Uri) : File? {
        val inputStream = contentResolver.openInputStream(uri)
        val file = createFile(applicationContext)

        if (inputStream != null) {
            val outputStream = FileOutputStream(file)

            val buf = ByteArray(1024)
            var len: Int = 0
            while (inputStream.read(buf).also { len = it } > 0) {
                outputStream.write(buf, 0, len)
            }

            outputStream.close()
            inputStream.close()
        }

        return file
    }

    /**
     * Open the gallery to select a profile picture
     */
    private fun pickImageFromGallery() {
        if (PermissionManager.checkPermission(Manifest.permission.READ_EXTERNAL_STORAGE, this)) {
            Intent().apply {
                type = "*/*"
                putExtra(Intent.ACTION_PICK, true)
                action = Intent.ACTION_GET_CONTENT
                startActivityForResult(this, REQUEST_PICK_IMAGE)
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            REQUEST_PICK_IMAGE -> {
                if (resultCode == Activity.RESULT_OK)
                    data?.data?.let {
                        CoroutineScope(Dispatchers.IO).launch {
                            split(it)
                        }
                    }
            }
        }
    }

    private suspend fun initProgress(max: Int) {
        CoroutineScope(Dispatchers.Main).launch {
            delay(50)
            progress_circular?.progress = 0
            progress_circular?.max = max
        }
    }

    private suspend fun updateProgress(progress: Int) {
        CoroutineScope(Dispatchers.Main).launch {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                progress_circular?.setProgress(progress, true)
            } else {
                progress_circular?.progress = progress
            }
        }
    }

    private suspend fun setMessage(type: Operation?) {
        CoroutineScope(Dispatchers.Main).launch {
            when (type) {
                Operation.SPLIT -> { message.text = getString(R.string.splitting_file) }
                Operation.JOIN -> { message.text = getString(R.string.joining_file) }
            }
        }
    }

    enum class Operation {
        SPLIT,
        JOIN
    }

    private suspend fun showSnackbarAlert(type: Operation) {
        withContext(Dispatchers.Main) {
            when (type) {
                Operation.SPLIT -> {
                    snackbarSuccess(getString(R.string.splitting_done))
                    message.text = getString(R.string.splitting_done)
                }
                Operation.JOIN -> {
                    snackbarSuccess(getString(R.string.joining_done))
                    message.text = getString(R.string.joining_done)
                }
            }
        }
    }
}