package com.janith.largefilesplitter

import android.app.Activity
import android.graphics.Color
import androidx.core.content.ContextCompat
import com.andrognito.flashbar.Flashbar

fun Activity.snackbarError(message: String) {
    Flashbar.Builder(this)
        .gravity(Flashbar.Gravity.TOP)
        .title("Error!")
        .message(message)
        .backgroundColor(Color.RED)
        .duration(Flashbar.DURATION_LONG)
        .build()
        .show()
}

fun Activity.snackbarSuccess(message: String){
    Flashbar.Builder(this)
        .gravity(Flashbar.Gravity.TOP)
        .title("Huzza!")
        .message(message)
        .backgroundColor(ContextCompat.getColor(this, R.color.green_dark))
        .duration(Flashbar.DURATION_LONG)
        .build()
        .show()
}

fun Activity.snackbarSuccess(titleRes: Int, messageRes: Int){
    Flashbar.Builder(this)
        .gravity(Flashbar.Gravity.TOP)
        .title(getString(titleRes))
        .message(getString(messageRes))
        .backgroundColor(ContextCompat.getColor(this, R.color.green_dark))
        .duration(Flashbar.DURATION_LONG)
        .build()
        .show()
}